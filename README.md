## Install requirements using pipenv

If you don't have pipenv, install it first

```
$ sudo pip install pipenv
```
or for Windows users: Open terminal as admin

```
$ pip install pipenv
```
Then activate virtual environment

```
$ pipenv shell
```

After this, start server:

```
$ python manage.py runserver
```

***

## If you need a development mod

```
$ cd frontend
$ npm install
```

## React builds

```
$ cd frontend
```

#### For development

```
$ npm run dev
```

#### For production

```
$ npm run build
```